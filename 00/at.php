<?php
/**
*版本:2.0
*爱特特爱探针
*官网:teai.me
*官网:aite.me
*邮箱:admin@teai.me
*邮箱:admin@aite.me
*QQ号:759234755
*检测基本环境信息
*检测模块加载情况
*检测服务器的CPU
*检测内存使用情况
*检测常见配置选项
*自定义检测的函数
*自定义检测配置项
*系统禁用函数检测
*爱特探针小巧实用
*探针信息实时更新
*对Mysql\Pgsql检测
**/
error_reporting(0);
function check_os() {
    $uname=php_uname();
    $uname=explode(" ",$uname);
    return $uname[0];
}
function check_ini($ini) {
    if(preg_match('/0|off/i',$x=ini_get($ini))) {
        return 'OFF';
    } elseif($x == null) {
        return 'NONE';
    } else {
        return $x == 1 ? 'ON' : $x;
}
}
function check_cpu() {
    if(is_readable('/proc/cpuinfo')) {
        $cpu=file_get_contents('/proc/cpuinfo');
        if(preg_match_all("/:.*?([0-9]+\.[0-9]+)/i",$cpu,$cpu)) {
            echo $cpu[1][0];
        } else {
            echo "错误";
        }
    } else {
        echo '禁查';
    }
}
function check_mem() {
    $meminfo=array();
    if(is_readable('/proc/meminfo')) {
        $mem=file_get_contents('/proc/meminfo');
        $imem=$mem;
        if(preg_match_all("/MemTotal.*?([0-9]+)/i",$imem,$mem)) {
            $meminfo['memtotal']=(int)($mem[1][0]/1024)." MB";
        } else {
            $meminfo['memtotal']='禁查';
        }
        if(preg_match_all("/MemFree.*?([0-9]+)/si",$imem,$mem)) {
            $meminfo['memfree']=(int)($mem[1][0]/1024)." MB";
        } else {
            $meminfo['memfree']='禁查';
        }
        if(preg_match_all("/SwapTotal.*?([0-9]+)/i",$imem,$mem)) {
            $meminfo['swaptotal']=(int)($mem[1][0]/1024)." MB";
        } else {
            $meminfo['swaptotal']='禁查';
        }
        if(preg_match_all("/SwapFree.*?([0-9]+)/si",$imem,$mem)) {
            $meminfo['swapfree']=(int)($mem[1][0]/1024)." MB";
        } else {
            $meminfo['swapfree']='禁查';
        }
        echo "物理内存大小:{$meminfo['memtotal']}<hr color=\"#ffaa00\" />物理内存空闲:{$meminfo['memfree']}<hr color=\"#ffaa00\" />SWAP虚拟内存:{$meminfo['swaptotal']}<hr color=\"#ffaa00\" />SWAP可用内存:{$meminfo['swapfree']}";
    } else {
        echo '<span style="color:#60def0">服务器禁止查询信息！</span>';
    }
}
function check_disk() {
    echo '磁盘大小:'; if(function_exists('disk_total_space')) { echo (int)(disk_total_space($_SERVER['DOCUMENT_ROOT'])/1024/1024).' MB'; } else { echo '禁止查询'; }
    echo '<hr color="#ffaa00" />';
    echo '磁盘空闲:'; if(function_exists('disk_free_space')) { echo (int)(disk_free_space($_SERVER['DOCUMENT_ROOT'])/1024/1024).' MB'; } else { echo '禁止查询'; }
    echo '<hr color="#ffaa00" />';
}
function check_uptime() {
    if(is_readable('/proc/uptime')) {
        $uptime=file_get_contents('/proc/uptime');
        $uptime=explode(" ",$uptime);
        $uptime=trim($uptime[0]);
        $min=$uptime/60;
        $hours=$min/60;
        $days=floor($hours/24);
        $hours=floor($hours-($days*24));
        $min=floor($min-($days*60*24)-($hours*60));
        echo $days."天".$hours."时".$min."分";
    } else {
        echo '禁查';
    }
}
function check_mail($mail) {
    if(function_exists('mail')) {
        if(mail($mail,"php E-mail","http://teai.me/\n\rhttp://{$_SERVER['HTTP_HOST']}/")) {
            return 'alert("系统函数Mail发邮成功");';
        } else {
            return 'alert("系统函数Mail发邮失败");';
        }
    } else {
        return 'alert("系统检测到Mail函数被禁用");';
    }
}
function check_function($fun) {
    if(preg_match('/^[a-z_]+[a-z0-9_\-]*/i',$fun)) {
        if(function_exists($fun)) {
            return '<script type="text/javascript">alert("系统函数 '.$fun.' 被支持！");</script>';
        } else {
            return '<script type="text/javascript">alert("系统函数 '.$fun.' 未找到！");</script>';
        }
    } else {
        return '<script type="text/javascript">alert("系统函数 '.$fun.' 不合法！");</script>';
    }
}
function check_extension() {
    $os=get_loaded_extensions();
    for($i=0;$i<count($os);$i++) {
        echo $os[$i];
        if(($i+1)>=7 and ($i+1)%7==0) { echo '<br />'; } else { echo '&nbsp;'; }
    }
}
function check_mysql($host,$user,$pass) {
    if(@mysql_connect($host,$user,$pass)) {
        return '<script type="text/javascript">alert("成功连接到MysqL数据库！");</script>';
    } else {
        return '<script type="text/javascript">alert("无法连接到MysqL数据库！");</script>';
    }
}
function check_pgsql($data) {
    if(@pg_connect($data)) {
        return '<script type="text/javascript">alert("成功连接到PgsqL数据库！");</script>';
    } else {
        return '<script type="text/javascript">alert("无法连接到PgsqL数据库！");</script>';
    }
}
function check_disable_functions() {
    $disable=ini_get('disable_functions');
    if($disable=="" || $disable==null) {
        echo "没有函数被系统禁用！";
    } else {
        $disable=explode(",",$disable);
        for($i=0;$i<count($disable);$i++) {
            echo $disable[$i];
            if(($i+1)>=7 and ($i+1)%7==0) { echo '<br />'; } else { echo '&nbsp;'; }
        }
    }
}
header("content-type:text/html;charset=UTF-8");
if(isset($_GET[page])) {
    if($_GET['page']=="date") {
        echo date("Y-m-d H:i:s");
    } elseif($_GET['page']=="phpinfo") {
        phpinfo();
    } elseif($_GET['page']=="cpuinfo") {
        check_cpu();
    } elseif($_GET['page']=="meminfo") {
        check_mem();
    } elseif($_GET['page']=="diskinfo"){
        check_disk();
    } elseif($_GET['page']=="uptime") {
        check_uptime();
    } elseif($_GET['page']=="download") {
        Header('Content-Type:application/octet-stream');
        header('accept-length:'.filesize($path));
        Header('Content-Disposition:attachment;filename=check.php');
        echo file_get_contents(__FILE__);
    }
    exit();
}
?>
<html>
<head>
<title>爱特探针</title>
<style type="text/css">
.aa{background-color:#8100ff;}
.bb{border:#9600ff solid 2px;}
</style>
<script type="text/javascript">
function idate() {
    date=new Date();
    h=date.getHours();
    m=date.getMinutes();
    s=date.getSeconds();
    document.getElementById("Date").innerHTML=h+":"+m+":"+s;
    setTimeout("idate()",1000);
}
function serverUp() {
    UP=new XMLHttpRequest();
    UP.onreadystatechange=function() {
        if(UP.readyState==4 && UP.status==200) {
            document.getElementById("serverUp").innerHTML=UP.responseText;
            setTimeout("serverUp()",10000);
        }
    }
    UP.open("GET","?page=uptime",true);
    UP.send();
}
function serverCpu() {
    CPU=new XMLHttpRequest();
    CPU.onreadystatechange=function() {
        if(CPU.readyState==4 && CPU.status==200) {
            document.getElementById("serverCpu").innerHTML=CPU.responseText;
            setTimeout("serverCpu()",2000);
        }
    }
    CPU.open("GET","?page=cpuinfo",true);
    CPU.send();
}
function serverMem() {
    MEM=new XMLHttpRequest();
    MEM.onreadystatechange=function() {
        if(MEM.readyState==4 && MEM.status==200) {
            document.getElementById("serverMem").innerHTML=MEM.responseText;
            setTimeout("serverMem()",2000);
        }
    }
    MEM.open("GET","?page=meminfo",true);
    MEM.send();
}
function serverDisk() {
    DISK=new XMLHttpRequest();
    DISK.onreadystatechange=function() {
        if(DISK.readyState==4 && DISK.status==200) {
            document.getElementById("serverDisk").innerHTML=DISK.responseText;
            setTimeout("serverDisk()",10000);
        }
    }
    DISK.open("GET","?page=diskinfo",true);
    DISK.send();
}
function serverDate() {
    XHR=new XMLHttpRequest();
    XHR.onreadystatechange=function() {
        if(XHR.readyState==4 && XHR.status==200) {
            document.getElementById("serverDate").innerHTML=XHR.responseText;
            setTimeout("serverDate()",1000);
        }
    }
    XHR.open("GET","?page=date",true);
    XHR.send();
}
</script>
</head>
<body>
<div class="aa">基本环境信息</div>
<div class="bb">
OS系统:<?php echo check_os(); ?>
<hr color="#ffaa00" />
PHP版本:<?php echo phpversion(); ?>
<hr color="#ffaa00" />
PHP信息:<a href="?page=phpinfo">PHPINFO</a>
<hr color="#ffaa00" />
CPU频率:<span id="serverCpu"></span>
<hr color="#ffaa00" />
<span id="serverDisk"></span>
运行时间:<span id="serverUp"></span>
<hr color="#ffaa00" />
系统时间:<span id="serverDate"></span>(<span id="Date"></span>)
<hr color="#ffaa00" />
配置文件:<?php echo php_ini_loaded_file(); ?>
<hr color="#ffaa00" />
主机域名:<?php echo $_SERVER['SERVER_NAME'].'-'.$_SERVER['HTTP_HOST'].'-'.$_SERVER['SERVER_ADDR']; ?>
<hr color="#ffaa00" />
文件地址:<?php echo __FILE__."&nbsp;&nbsp;".$_SERVER['DOCUMENT_ROOT']; ?>
<hr color="#ffaa00" />
内核信息:<?php echo php_uname(); ?>
</div>
<div class="aa">已加载的模块</div>
<div class="bb">
<span style="color:#60def0"><?php echo check_extension(); ?></span>
</div>
<div class="aa">系统禁用函数</div>
<div class="bb">
<span style="color:#60def0"><?php echo check_disable_functions(); ?></span>
</div>
<div class="aa">系统内存信息</div>
<div class="bb">
<span id="serverMem"></span>
</div>
<div class="aa">系统配置选项</div>
<div class="bb">
PHP的短标签:<?php echo check_ini('short_open_tag'); ?>
<hr color="#ffaa00" />
打开远程文件:<?php echo check_ini('allow_url_fopen'); ?>
<hr color="#ffaa00" />
浮点有效位数:<?php echo check_ini('precision'); ?>
<hr color="#ffaa00" />
脚本超时时间:<?php echo ini_get('max_execution_time'); ?>
<hr color="#ffaa00" />
脚本最大占用内存:<?php echo check_ini('memory_limit'); ?>
<hr color="#ffaa00" />
上传文件的大小限制:<?php echo check_ini('upload_max_filesize'); ?>
<hr color="#ffaa00" />
POST方法提交最大限制:<?php echo check_ini('post_max_size'); ?>
</div>
<div class="aa">系统函数检测</div>
<div class="bb">
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
函数:<input type="text" name="fun" /><br />
<input type="submit" value="CHECK" /><?php if(isset($_POST['fun'])) { echo check_function($_POST['fun']); } ?>
</form>
</div>
<div class="aa">系统选项检测</div>
<div class="bb">
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
选项:<input type="text" name="ini" /><br />
<input type="submit" value="CHECK" /><?php if(isset($_POST['ini'])) { echo '<script type="text/javascript">alert("选项 "+"'.$_POST['ini'].'".toUpperCase()+" 值为 '.check_ini($_POST['ini']).'");</script>'; } ?>
</form>
</div>
<div class="aa">系统发信检测</div>
<div class="bb">
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
电邮:<input type="text" name="mail" /><br />
<input type="submit" value="CHECK" /><?php if(isset($_POST['mail'])) { echo '<script type="text/javascript">'.check_mail($_POST['mail']).'</script>'; } ?>
</form>
</div>
<div class="aa">数据库服务器</div>
<div class="bb">
<?php if(function_exists('mysql_connect')) { ?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
主机:<input type="text" name="host" value="127.0.0.1:3306" /><br />
用户:<input type="text" name="user" /><br />
密码:<input type="password" name="pass" /><br />
<input type="submit" value="检测MYSQL" /><?php if(isset($_POST['host']) and isset($_POST['user']) and isset($_POST['pass'])) { echo '<span style="background-color:#663399">'.check_mysql($_POST['host'],$_POST['user'],$_POST['pass']).'</span>'; } ?>
</form>
<?php } else { echo '<span style="color:#60def0">未找到Mysql扩展!</span>'; } ?>
<hr color="#ffaa00" />
<?php if(function_exists('pg_connect')) { ?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
连接:<input type="text" name="pg" value="host=127.0.0.1 port=5432 dbname=postgres user=*** password=***" /><br />
<input type="submit" value="检测POSTGRESQL" /><?php if(isset($_POST['pg'])) { echo '<span style="background-color:#663399">'.check_pgsql($_POST['pg']).'</span>'; } ?>
</form>
<?php } else { echo '<span style="color:#60def0">未找到Postgresql扩展!</span>'; } ?>
</div>
<div class="aa">爱特特爱©原创动力</div>
<div class="bb">
<a href="?page=download">免费获取适用于UNIX系统的爱特探针</a>
</div>
<script type="text/javascript">
window.onload=function() {
idate();
serverUp();
serverCpu();
serverMem();
serverDisk();
serverDate();
}
</script>
</body>
</html>